package presentation;

import java.lang.reflect.Field;
import java.util.List;

import javax.swing.JTable;

public class Reflection {
	public static JTable createJTable(List<Object> object,Object[] atribute) {
		Object [][] v = new Object[object.size()][atribute.length];
		int i=0;
		int j=0;
		for(Object it : object){
			for (Field field : it.getClass().getDeclaredFields()) {
				field.setAccessible(true); 
				Object obj;
				
				try {
					obj = field.get(it);
					v[i][j]=obj;
					j++;
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}			
			}
			i++;
			j=0;
		}
		
		JTable t = new JTable(v,atribute);
		return t;
	}
}
