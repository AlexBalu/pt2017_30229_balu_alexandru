package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

public class ConnectionFactory {
	private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String DBURL = "jdbc:mysql://localhost:3306/schooldb?verifyServerCertificate=false&useSSL=true";
	private static final String USER = "root";
	private static final String PASS = "root";
	private static Connection connection = null;
	private static Connection conn = null;
	private static ConnectionFactory singleInstance = new ConnectionFactory();

	protected ConnectionFactory() {

		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	protected Connection createConnection() {
		// Connection connection = null;
		try {
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/schooldb", "root", "root");

		} catch (SQLException e) {
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return null;
		}

		if (connection != null) {
			System.out.println("You made it, take control your database now!");
			return connection;
		} else {
			System.out.println("Failed to make connection!");
			return null;
		}
	}
	/*
	 * public static Connection createConnection1() {
	 * 
	 * try { Class.forName("com.mysql.jdbc.Driver"); conn =
	 * DriverManager.getConnection(DBURL, USER, PASS); } catch (SQLException e)
	 * { System.out.println(e.getMessage()); } catch (ClassNotFoundException e)
	 * { System.out.println(e.getMessage()); } return conn; }
	 */

	public static Connection getConnection() {
		return connection;

	}

	public static void close(Connection connection) {
		if (connection != null)
			ConnectionFactory.close(connection);

	}

	public static void close(PreparedStatement statement) {
		if(statement!=null)
		ConnectionFactory.close(statement);
	}

	public static void close(ResultSet resultSet) {
		if (resultSet!=null)
		ConnectionFactory.close(resultSet);
	}

}