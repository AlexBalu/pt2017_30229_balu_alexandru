package dao;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.mysql.jdbc.Field;
import com.sun.javafx.property.adapter.PropertyDescriptor;

import model.Produs;

public class ProdusDAO {
	 private static final Logger logger = Logger.getLogger(ProdusDAO.class);
	    /** The query for find by id. */
	    private static final String FIND_BY_ID = "SELECT nume,cantitate,pret,id_producator FROM produs WHERE id_produs = ?";
	    /** The query for creation. */
	    private static final String CREATE_QUERY = "INSERT INTO produs (id_produs,nume,cantitate,pret,id_producator) VALUES (?,?,?,?,?)";
	    /** The query for read. */
	    private static final String READ_QUERY = "SELECT * FROM produs WHERE id_produs = ?";
	    /** The query for update. */
	    private static final String UPDATE_QUERY = "UPDATE produs SET  nume=?,cantitate=?,pret=?,id_producator=? WHERE id_produs = ?";
	    /** The query for delete. */
	    private static final String DELETE_QUERY = "DELETE FROM produs WHERE id_produs = ?";
	    /** The query for select all. */
	    private static final String SELECTALL_QUERY = "SELECT * FROM produs";
	    
	    private ConnectionFactory c = new ConnectionFactory();
	    
	    public Produs findById(int produsId) {
			Produs toReturn = null;
			Connection conn=null;
			PreparedStatement findStatement = null;
			ResultSet rs = null;
			
			try {
				conn = c.createConnection();
				findStatement = conn.prepareStatement(FIND_BY_ID);
				findStatement.setLong(1, produsId);
				rs = findStatement.executeQuery();
				rs.next();
				
				String nume = rs.getString("nume");
				int cantitate = rs.getInt("cantitate");
				int pret = rs.getInt("pret");
				int id_producator = rs.getInt("id_producator");
				toReturn = new Produs(produsId,nume,cantitate, pret, id_producator);
				
			} catch (SQLException e) {
				System.out.println("ProdusDAO:findById " + e.getMessage());
				//logger.log(Level.WARNING,"ProdusDAO:findById " + e.getMessage());
			} finally {
				 try {
		                rs.close();
		            } catch (Exception rse) {
		               // logger.error(rse.getMessage());
		            }
		            try {
		                findStatement.close();
		            } catch (Exception sse) {
		               // logger.error(sse.getMessage());
		            }
		            try {
		                conn.close();
		            } catch (Exception cse) {
		             //   logger.error(cse.getMessage());
		            }
			}
			return toReturn;
		}
	    
	    public int create(Produs produs) {
	        Connection conn = null;
	        PreparedStatement preparedStatement = null;
	        ResultSet result = null;
	        try {
	            conn = c.createConnection();
	            preparedStatement = conn.prepareStatement(CREATE_QUERY,Statement.RETURN_GENERATED_KEYS);
	            preparedStatement.setInt(1, produs.getId_produs());
	            preparedStatement.setString(2, produs.getNume());
	            preparedStatement.setInt(3, produs.getCantitate());
	            preparedStatement.setInt(4, produs.getPret());
	            preparedStatement.setInt(5, produs.getId_producator());
	      
	            preparedStatement.executeUpdate();
	           result = preparedStatement.getGeneratedKeys();
	 
	            if (result.next() && result != null) {
	                return result.getInt(1);
	            } else {
	                return -1;
	            }
	        } catch (SQLException e) {
	        	System.out.println("Eroare la create in ProdusDAO"+e.getMessage());
	            //logger.error(e.getMessage());
	        } finally {
	            try {
	                result.close();
	            } catch (Exception rse) {
	               // logger.error(rse.getMessage());
	            }
	            try {
	                preparedStatement.close();
	            } catch (Exception sse) {
	               // logger.error(sse.getMessage());
	            }
	            try {
	                conn.close();
	            } catch (Exception cse) {
	             //   logger.error(cse.getMessage());
	            }
	        }
	 
	        return -1;
	    }
	 
	    public Produs read(int id) {
	        Produs produs = null;
	        Connection conn = null;
	        PreparedStatement preparedStatement = null;
	        ResultSet result = null;
	        try {
	            conn = c.createConnection();
	            preparedStatement = conn.prepareStatement(READ_QUERY);
	            preparedStatement.setInt(1,id);
	            preparedStatement.execute();
	            result = preparedStatement.getResultSet();
	 
	            if (result.next() && result != null) {
	                produs = new Produs(result.getInt(1),result.getString(2),result.getInt(3),result.getInt(4),result.getInt(5));
	           
	            } else {
	                // TODO
	            	System.out.println("Result NULL in ProdusDAO");
	            }
	        } catch (SQLException e) {
	            logger.error(e.getMessage());
	        } finally {
	            try {
	                result.close();
	            } catch (Exception rse) {
	                logger.error(rse.getMessage());
	            }
	            try {
	                preparedStatement.close();
	            } catch (Exception sse) {
	                logger.error(sse.getMessage());
	            }
	            try {
	                conn.close();
	            } catch (Exception cse) {
	                logger.error(cse.getMessage());
	            }
	        }
	 
	        return produs;
	    }
	 
	    public boolean update(Produs produs) {
	        Connection conn = null;
	        PreparedStatement preparedStatement = null;
	        try {
	            conn = c.createConnection();
	            preparedStatement = conn.prepareStatement(UPDATE_QUERY);
	            
	           
	            preparedStatement.setString(1, produs.getNume());
	            preparedStatement.setInt(2, produs.getCantitate());
	            preparedStatement.setInt(3, produs.getPret());
	            preparedStatement.setInt(4, produs.getId_producator());
	            preparedStatement.setInt(5, produs.getId_produs());
	            preparedStatement.executeUpdate();
	            return true;
	        } catch (SQLException e) {
	            logger.error(e.getMessage());
	        } finally {
	            try {
	                preparedStatement.close();
	            } catch (Exception sse) {
	                logger.error(sse.getMessage());
	            }
	            try {
	                conn.close();
	            } catch (Exception cse) {
	                logger.error(cse.getMessage());
	            }
	        }
	        return false;
	    }
	 
	    public boolean delete(int id) {
	        Connection conn = null;
	        PreparedStatement preparedStatement = null;
	        try {
	            conn = c.createConnection();
	            preparedStatement = conn.prepareStatement(DELETE_QUERY);
	            preparedStatement.setInt(1, id);
	            preparedStatement.execute();
	            return true;
	        } catch (SQLException e) {
	            logger.error(e.getMessage());
	        } finally {
	            try {
	                preparedStatement.close();
	            } catch (Exception sse) {
	                logger.error(sse.getMessage());
	            }
	            try {
	                conn.close();
	            } catch (Exception cse) {
	                logger.error(cse.getMessage());
	            }
	        }
	        return false;
	    }

	    public List<Produs> selectAll() {
	    	Connection conn=null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			List<Produs> list = new ArrayList<Produs>();
			
			String query = SELECTALL_QUERY;
			try {
				conn = c.createConnection();
				statement = conn.prepareStatement(query);
				resultSet = statement.executeQuery();
				try {
					while (resultSet.next()) {
					  list.add(new Produs(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3),resultSet.getInt(4),resultSet.getInt(5)));
					}
				} catch (SecurityException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				} 
				return list;
				
			} catch (SQLException e) {
				System.out.println("DAO:findById " + e.getMessage());
			} finally {
				 try {
		                statement.close();
		            } catch (Exception sse) {
		                logger.error(sse.getMessage());
		            }
		            try {
		                conn.close();
		            } catch (Exception cse) {
		                logger.error(cse.getMessage());
		            }
			}
			return null;
		}
}
