package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import model.Producator;

public class ProducatorDAO {
	   private static final Logger logger = Logger.getLogger(ProducatorDAO.class);
	    /** The query for find by id. */
	    private static final String FIND_BY_ID = "SELECT nume_producator,id_produs FROM producator WHERE id_producator = ?";
	    /** The query for creation. */
	    private static final String CREATE_QUERY = "INSERT INTO producator (id_producator,nume_producator,id_produs) VALUES (?,?,?)";
	    /** The query for read. */
	    private static final String READ_QUERY = "SELECT * FROM producator WHERE id_producator = ?";
	    /** The query for update. */
	    private static final String UPDATE_QUERY = "UPDATE producator SET  nume_producator=?,id_produs=? WHERE id_producator = ?";
	    /** The query for delete. */
	    private static final String DELETE_QUERY = "DELETE FROM producator WHERE id_producator = ?";
	    
	    private ConnectionFactory c = new ConnectionFactory();
	    
	    public Producator findById(int producatorId) {
			Producator toReturn = null;
			Connection conn=null;
		//	Connection dbConnection = ConnectionFactory.getConnection();
			PreparedStatement findStatement = null;
			ResultSet rs = null;
			
			try {
				conn = c.createConnection();
				findStatement = conn.prepareStatement(FIND_BY_ID);
				findStatement.setLong(1, producatorId);
				rs = findStatement.executeQuery();
				rs.next();
				
				String nume_producator = rs.getString("nume_producator");
				int id_produs = rs.getInt("id_produs");
				toReturn = new Producator(producatorId,nume_producator,id_produs);
				
			} catch (SQLException e) {
				System.out.println("ProducatorDAO:findById " + e.getMessage());
				//logger.log(Level.WARNING,"ProducatorDAO:findById " + e.getMessage());
			} finally {
				 try {
		                rs.close();
		            } catch (Exception rse) {
		               // logger.error(rse.getMessage());
		            }
		            try {
		                findStatement.close();
		            } catch (Exception sse) {
		               // logger.error(sse.getMessage());
		            }
		            try {
		                conn.close();
		            } catch (Exception cse) {
		             //   logger.error(cse.getMessage());
		            }
			}
			return toReturn;
		}
	    
	    public int create(Producator producator) {
	        Connection conn = null;
	        PreparedStatement preparedStatement = null;
	        ResultSet result = null;
	        try {
	            conn = c.createConnection();
	            preparedStatement = conn.prepareStatement(CREATE_QUERY,Statement.RETURN_GENERATED_KEYS);
	            preparedStatement.setInt(1, producator.getId_producator());
	            preparedStatement.setString(2, producator.getNume_producator());
	            preparedStatement.setInt(3, producator.getId_produs());
	            System.out.println("id produssssss = "+producator.getId_produs());
	            preparedStatement.executeUpdate();
	           result = preparedStatement.getGeneratedKeys();
	 
	            if (result.next() && result != null) {
	                return result.getInt(1);
	            } else {
	                return -1;
	            }
	        } catch (SQLException e) {
	        	System.out.println("Eroare la create in ProducatorDAO"+e.getMessage());
	            //logger.error(e.getMessage());
	        } finally {
	            try {
	                result.close();
	            } catch (Exception rse) {
	               // logger.error(rse.getMessage());
	            }
	            try {
	                preparedStatement.close();
	            } catch (Exception sse) {
	               // logger.error(sse.getMessage());
	            }
	            try {
	                conn.close();
	            } catch (Exception cse) {
	             //   logger.error(cse.getMessage());
	            }
	        }
	 
	        return -1;
	    }
	 
	    public Producator read(int id) {
	        Producator producator = null;
	        Connection conn = null;
	        PreparedStatement preparedStatement = null;
	        ResultSet result = null;
	        try {
	            conn = c.createConnection();
	            preparedStatement = conn.prepareStatement(READ_QUERY);
	            preparedStatement.setInt(1,id);
	            preparedStatement.execute();
	            result = preparedStatement.getResultSet();
	 
	            if (result.next() && result != null) {
	                producator = new Producator(result.getInt(1),result.getString(2),result.getInt(3));
	           
	            } else {
	                // TODO
	            	System.out.println("Result NULL in ProducatorDAO");
	            }
	        } catch (SQLException e) {
	            logger.error(e.getMessage());
	        } finally {
	            try {
	                result.close();
	            } catch (Exception rse) {
	                logger.error(rse.getMessage());
	            }
	            try {
	                preparedStatement.close();
	            } catch (Exception sse) {
	                logger.error(sse.getMessage());
	            }
	            try {
	                conn.close();
	            } catch (Exception cse) {
	                logger.error(cse.getMessage());
	            }
	        }
	 
	        return producator;
	    }
	 
	    public boolean update(Producator producator) {
	        Connection conn = null;
	        PreparedStatement preparedStatement = null;
	        try {
	            conn = c.createConnection();
	            preparedStatement = conn.prepareStatement(UPDATE_QUERY);
	            
	           
	            preparedStatement.setString(1, producator.getNume_producator());
	            preparedStatement.setInt(2, producator.getId_produs());
	            preparedStatement.setInt(3, producator.getId_producator());
	            preparedStatement.executeUpdate();
	            return true;
	        } catch (SQLException e) {
	            logger.error(e.getMessage());
	        } finally {
	            try {
	                preparedStatement.close();
	            } catch (Exception sse) {
	                logger.error(sse.getMessage());
	            }
	            try {
	                conn.close();
	            } catch (Exception cse) {
	                logger.error(cse.getMessage());
	            }
	        }
	        return false;
	    }
	 
	    public boolean delete(int id) {
	        Connection conn = null;
	        PreparedStatement preparedStatement = null;
	        try {
	            conn = c.createConnection();
	            preparedStatement = conn.prepareStatement(DELETE_QUERY);
	            preparedStatement.setInt(1, id);
	            preparedStatement.execute();
	            return true;
	        } catch (SQLException e) {
	            logger.error(e.getMessage());
	        } finally {
	            try {
	                preparedStatement.close();
	            } catch (Exception sse) {
	                logger.error(sse.getMessage());
	            }
	            try {
	                conn.close();
	            } catch (Exception cse) {
	                logger.error(cse.getMessage());
	            }
	        }
	        return false;
	    }
}
