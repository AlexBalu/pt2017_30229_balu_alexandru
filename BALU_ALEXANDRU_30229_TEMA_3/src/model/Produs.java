package model;

public class Produs {
private int id_produs;
private String nume;
private int cantitate;
private int pret;
private int id_producator;


public Produs(int id_produs, String nume, int cantitate,int pret,int id_producator){
	// TODO Auto-generated constructor stub
this.id_produs = id_produs;
this.nume = nume;
this.cantitate = cantitate;
this.pret = pret;
this.id_producator = id_producator;

}

public int getId_produs() {
	return id_produs;
}

public void setId_produs(int id_produs) {
	this.id_produs = id_produs;
}

public String getNume() {
	return nume;
}
public void setNume(String nume) {
	this.nume = nume;
}
public int getCantitate() {
	return cantitate;
}
public void setCantitate(int cantitate) {
	this.cantitate = cantitate;
}
public int getPret() {
	return pret;
}
public void setPret(int pret) {
	this.pret = pret;
}
public int getId_producator() {
	return id_producator;
}
public void setId_producator(int id_producator) {
	this.id_producator = id_producator;
}
}
